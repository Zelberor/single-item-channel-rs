mod base;
mod tests;

use std::sync::{Arc, Condvar, Mutex};

use base::Base;
pub use base::{ChannelError, ChannelResult};

pub fn channel<T: Send>() -> (Sender<T>, Receiver<T>) {
	let mutex = Arc::new(Mutex::new(None));
	let given_condvar = Arc::new(Condvar::new());
	let given_weak = Arc::downgrade(&given_condvar);
	let taken_condvar = Arc::new(Condvar::new());
	let taken_weak = Arc::downgrade(&taken_condvar);

	let sender = Sender {
		0: Base::new(mutex.clone(), taken_weak, given_condvar),
	};
	let receiver = Receiver {
		0: Base::new(mutex, given_weak, taken_condvar),
	};
	(sender, receiver)
}

#[derive(Debug)]
pub struct Sender<T>(Base<T>)
where
	T: Send,
	Self: Send;

impl<T> Sender<T>
where
	T: Send,
{
	pub fn send(&self, item: T) -> ChannelResult<()> {
		if !self.0.has_peer() {
			return Err(ChannelError::NoPeer);
		}

		{
			let mut lock = self.0.get_item().lock()?;
			*lock = Some(item);
		}
		self.0.get_event_sender().notify_one();
		Ok(())
	}

	pub fn wait_taken(&self) -> ChannelResult<()> {
		let mut lock = self.0.get_item().lock()?;
		while lock.is_some() {
			let event_receiver = self.0.get_event_receiver()?;
			lock = event_receiver.wait(lock)?;
		}
		Ok(())
	}
}

#[derive(Debug)]
pub struct Receiver<T>(Base<T>)
where
	T: Send,
	Self: Send;

impl<T> Receiver<T>
where
	T: Send,
{
	pub fn try_receive(&self) -> ChannelResult<Option<T>> {
		let option;
		{
			let mut option_lock = self.0.get_item().lock()?;
			option = option_lock.take();
			if option.is_none() && !self.0.has_peer() {
				return Err(ChannelError::NoPeer);
			}
			std::mem::drop(option_lock);
		}

		self.0.get_event_sender().notify_one();
		Ok(option)
	}

	pub fn receive(&self) -> ChannelResult<T> {
		let result;
		{
			let mut option_lock = self.0.get_item().lock()?;
			while option_lock.is_none() {
				let event_receiver = self.0.get_event_receiver()?;
				option_lock = event_receiver.wait(option_lock)?;
			}
			result = option_lock.take().expect("While makes sure option_lock is Some.")
		}
		self.0.get_event_sender().notify_one();
		Ok(result)
	}
}
