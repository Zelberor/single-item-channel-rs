#[cfg(test)]
mod tests {
	use std::thread;

	use crate::*;

	#[test]
	fn send_and_receive() {
		let (tx, rx) = channel();

		let value = 42;

		let join = thread::spawn(move || {
			if let Err(err) = tx.send(value) {
				panic!("Failed to send value: {}", err);
			}

			if let Err(err) = tx.wait_taken() {
				panic!("Failed to wait: {}", err);
			}
		});

		let received = match rx.receive() {
			Ok(v) => v,
			Err(err) => panic!("Failed to receive value: {}", err),
		};

		assert_eq!(received, value);

		join.join().unwrap();
	}
}
