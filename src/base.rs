use std::fmt::Display;
use std::sync::{Arc, Condvar, Mutex, PoisonError, Weak};

pub type ChannelResult<T> = Result<T, ChannelError>;
#[derive(Debug, Clone, Copy)]
pub enum ChannelError {
	NoPeer,
	PoisonError,
}

impl<T> From<PoisonError<T>> for ChannelError {
	fn from(_: PoisonError<T>) -> Self {
		ChannelError::PoisonError
	}
}

impl Display for ChannelError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"{}",
			match self {
				ChannelError::NoPeer => "No channel peer",
				ChannelError::PoisonError => "Channel mutex poisoned",
			}
		)
	}
}

#[derive(Debug)]
pub struct Base<T>
where
	Self: Send,
{
	item: Arc<Mutex<Option<T>>>,
	/// Receives events
	event_receiver: Weak<Condvar>,
	// Sends events
	event_sender: Option<Arc<Condvar>>,
}

impl<T> Base<T>
where
	Self: Send,
{
	pub fn new(item_mutex: Arc<Mutex<Option<T>>>, event_receiver: Weak<Condvar>, event_sender: Arc<Condvar>) -> Self {
		Base {
			item: item_mutex,
			event_receiver,
			event_sender: Some(event_sender),
		}
	}

	pub fn has_peer(&self) -> bool {
		self.event_receiver.strong_count() != 0
	}

	pub fn get_item(&self) -> &Arc<Mutex<Option<T>>> {
		&self.item
	}

	pub fn get_event_receiver(&self) -> ChannelResult<Arc<Condvar>> {
		match self.event_receiver.upgrade() {
			Some(r) => Ok(r),
			None => Err(ChannelError::NoPeer),
		}
	}

	pub fn get_event_sender(&self) -> &Arc<Condvar> {
		self.event_sender.as_ref().expect("event_sender must always be Some exept when self is being dropped.")
	}
}

impl<T> Drop for Base<T>
where
	Self: Send,
{
	fn drop(&mut self) {
		// Lock the Mutex -> No peer can start waiting for events
		if let Ok(guard) = self.item.lock() {
			if let Some(event_sender) = self.event_sender.take() {
				event_sender.notify_all();
			} // Take the event sender so it gets dropped now and weak references of the peers will be invalid
			std::mem::drop(guard);
		}
	}
}
